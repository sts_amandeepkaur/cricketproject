from django.shortcuts import render
from datetime import datetime
from dateutil import tz
import requests,random
from django.http import HttpResponse
from cricketapp.models import feedback

def index(request):
    data = requests.get('https://cricapi.com/api/matches?apikey=sPB9BV10yQQldWSILDb4SraTNVp2')
    api = data.json()
    all = []
    started=[]
    for i in api['matches']:
        img = random.randint(100,105)
        img1 = random.randint(100,105)
        t=convertTime('UTC','Asia/Kolkata',i['dateTimeGMT'][:-5].replace('T',' '))
        time=str(t)[:-6]
        dict1={}
        dict={}
        if i['matchStarted'] == True:
            dict1 = {
            'match_id':i['unique_id'],
            'date':time,
            'team1':i['team-1'],
            'team2':i['team-2'],
            'type':i['type'],
            'logot1':str(img)+'.png',
            'logot2':str(img1)+'.png',
            'squad':i['squad'],
            }
            if 'winner_team' in i:
                dict1.update({'winner_team':i['winner_team']})
            if 'toss_winner_team' in i:
                dict1.update({'toss_winner_team':i['toss_winner_team']})
            started.append(dict1)
        else:
            dict = {
            'match_id':i['unique_id'],
            'date':time,
            'team1':i['team-1'],
            'team2':i['team-2'],
            'type':i['type'],
            'logot1':str(img)+'.png',
            'logot2':str(img1)+'.png',
            'squad':i['squad'],
            }
            all.append(dict)
    return render(request,'index.html',{'all_matches':all,'recents':started,'total':len(all),'rtotal':len(started)})

def convertTime(frm,to,dt):
    # METHOD 1: Hardcode zones:
    from_zone = tz.gettz(frm)
    to_zone = tz.gettz(to)

    # METHOD 2: Auto-detect zones:
    from_zone = tz.tzutc()
    to_zone = tz.tzlocal()

    # utc = datetime.utcnow()
    utc = datetime.strptime(dt, '%Y-%m-%d %H:%M:%S')

    # Tell the datetime object that it's in UTC time zone since 
    # datetime objects are 'naive' by default
    utc = utc.replace(tzinfo=from_zone)

    # Convert time zone
    central = utc.astimezone(to_zone)
    return central

def scoreboard(request):
    id = request.GET['match_id']
    lg = request.GET['l1']
    lgt = request.GET['l2']
    wt='team' 
    
    api = requests.get('https://cricapi.com/api/fantasySummary?apikey=sPB9BV10yQQldWSILDb4SraTNVp2&unique_id='+id)
    all = api.json()
    t=convertTime('UTC','Asia/Kolkata',all['dateTimeGMT'][:-5].replace('T',' '))
    date=str(t)[:-6]
    fielding=all['data']['fielding']
    batting=all['data']['batting']
    bowling=all['data']['bowling']
    team = all['data']['team']
    m_type=all['type']
    data = all['data']
    man = all['data']['man-of-the-match']
    toss = all['data']['toss_winner_team']
    if all['data']['winner_team']:
        wt = all['data']['winner_team']
    

    tt =0
    ttwo=0
    gap=0
    for i in batting[0]['scores']:
        if type(i['R'])==type(1):
            tt += int(i['R'])
        if i['batsman']=="Extras":
            e= i['detail'].split(' ')
            tt += int(e[0])
    for j in batting[1]['scores']:
        if type(j['R'])==type(1):
            ttwo += int(j['R'])
        if j['batsman']=="Extras":
            e= j['detail'].split(' ')
            ttwo += int(e[0])
    if tt>ttwo:
        gap = tt-ttwo
        lgo = lg
    else:
        gap =ttwo-tt
        lgo = lgt
    return render(request,'scoreboard.html',{'team':team,'bowling':bowling,'batting':batting,'team1':tt,'team2':ttwo,'gap':gap,'lgo':lgo,'wt':wt,'man':man,'data':data,'type':m_type,'date':date,'toss':toss})

def player_profile(request):
    id = request.GET['id']
    data = requests.get('https://cricapi.com/api/playerStats?apikey=sPB9BV10yQQldWSILDb4SraTNVp2&pid='+str(id))
    api = data.json()
    teams = api['majorTeams'].split(',')
    bowling = api['data']['bowling']
    batting = api['data']['batting']
    return render(request,'profile.html',{'info':api,'teams':teams,'batting':batting,'bowling':bowling})

def squad(request):
    id = request.GET['id']
    img1 = request.GET['l1']
    img2 = request.GET['l2']
    sqa = requests.get('https://cricapi.com/api/fantasySquad?apikey=sPB9BV10yQQldWSILDb4SraTNVp2&unique_id='+str(id)).json()
    tt1 = len(sqa['squad'][0]['players'])
    tt2 = len(sqa['squad'][1]['players'])
    return render(request,'squad.html',{'logot1':img1,'logot2':img2,'sqa':sqa,'t1':tt1,'t2':tt2})

def search(request):
    if request.method == 'POST':
        name = request.POST['name']
        api = requests.get('https://cricapi.com/api/playerFinder?apikey=sPB9BV10yQQldWSILDb4SraTNVp2&name='+name).json()
        return render(request,'search.html',{'data':api['data'],'total':len(api['data']),'sr':name})    
    return render(request,'search.html')

def upcoming(request):
    api = requests.get('https://cricapi.com/api/matchCalendar?apikey=sPB9BV10yQQldWSILDb4SraTNVp2').json()
    return render(request,'upcoming.html',{'data':api['data']})

def old_matches(request):
    all = []
    api = requests.get('https://cricapi.com/api/cricket?apikey=sPB9BV10yQQldWSILDb4SraTNVp2').json()
    for i in api["data"]:
        ic1 =random.randint(100,105)
        ic2 =random.randint(100,105)
        match = {}
        arr = i['description'].split('v')
        match['team1']=arr[0]
        match['team2']=arr[1]
        match['ic1']=str(ic1)+'.png'
        match['ic2']=str(ic2)+'.png'
        all.append(match)
    return render(request,'old.html',{'all':all})

def contact(request):
    if request.method == 'POST':
        n = request.POST['name']
        e = request.POST['email']
        m = request.POST['message']

        all_data = feedback(name=n,email=e,message=m)
        all_data.save()
        return render(request,'contact.html',{'status':'Dear '+n+' Thanks for your feedback!!'})
    return render(request,'contact.html')